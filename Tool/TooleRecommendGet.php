<?php
/**
 * 查询推广卡片推荐内容
 * User: yueguang
 * Date: 2022/4/1
 * Time: 11:37
 */

namespace Tool;

use core\Helper\RequestCheckUtil;
use core\Profile\RpcRequest;

class TooleRecommendGet extends RpcRequest
{
    protected $url = '/2/tools/promotion_card/recommend/get/';
    protected $method = 'GET';
    protected $content_type = 'application/json';

    /**
     * 广告主id
     * @var int $advertiser_id
     */
    protected $advertiser_id;

    /**
     * @param mixed $args
     * @return $this
     */
    public function setArgs($args)
    {
        foreach ($args as $key => $value) {
            $this->params[$key] = $this->{$key} = $value;
        }
        return $this;
    }

    /**
     * 处理url 拼接参数
     * @param $interlinkage
     * @return $this
     */
    public function montageUrl($interlinkage){
        $this->url =  $this->url  . $interlinkage;
        return $this;
    }

    /**
     * @throws \core\Exception\InvalidParamException
     */
    public function check()
    {
        RequestCheckUtil::checkNotNull($this->advertiser_id, 'advertiser_id');
    }
}
